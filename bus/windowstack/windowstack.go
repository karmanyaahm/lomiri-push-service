/*
 Copyright 2014 Canonical Ltd.

 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License version 3, as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranties of
 MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Package windowstack retrieves information about the windowstack
// using Lomiri's dbus interface
package windowstack

import (
	"gitlab.com/ubports/core/lomiri-push-service/bus"
	"gitlab.com/ubports/core/lomiri-push-service/click"
	"gitlab.com/ubports/core/lomiri-push-service/logger"
)

// Well known address for the WindowStack API
var BusAddress bus.Address = bus.Address{
	Interface: "com.lomiri.WindowStack",
	Path:      "/com/lomiri/WindowStack",
	Name:      "com.lomiri.WindowStack",
}

type WindowsInfo struct {
	WindowId uint32
	AppId    string // in the form "app.example" or "webbrowser-app"
	Focused  bool
	Stage    uint32
}

// WindowStack encapsulates info needed to call out to the WindowStack API
type WindowStack struct {
	bus bus.Endpoint
	log logger.Logger
}

// New returns a new WindowStack that'll use the provided bus.Endpoint
func New(endp bus.Endpoint, log logger.Logger) *WindowStack {
	return &WindowStack{endp, log}
}

// GetWindowStack returns the window stack state
func (stack *WindowStack) GetWindowStack() []WindowsInfo {
	var wstack []WindowsInfo
	err := stack.bus.Call("GetWindowStack", bus.Args(), &wstack)
	if err != nil {
		stack.log.Errorf("GetWindowStack call returned %v", err)
	}
	return wstack
}

func (stack *WindowStack) IsAppFocused(AppId *click.AppId) bool {
	for _, winfo := range stack.GetWindowStack() {
		if winfo.Focused && winfo.AppId == AppId.Base() {
			return true
		}
	}
	return false
}
